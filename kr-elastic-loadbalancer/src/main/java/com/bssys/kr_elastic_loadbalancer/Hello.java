package com.bssys.kr_elastic_loadbalancer;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
