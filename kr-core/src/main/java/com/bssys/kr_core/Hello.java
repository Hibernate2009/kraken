package com.bssys.kr_core;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
